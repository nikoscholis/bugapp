import enum
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import asc, desc
from sqlalchemy import exc as sqlalchemy_exc
from flask_marshmallow import Marshmallow
from flask_restful import Api, Resource, reqparse
from collections import namedtuple
import factory
import factory.fuzzy
from datetime import datetime, timezone

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
ma = Marshmallow(app)
api = Api(app)

# class BugStatus(enum.Enum):
#     CREATED = 'created'
#     PENDING = 'pending'
#     IN_PROGRESS = 'in_progress'
#     DONE = 'done'
#     DELETED = 'deleted'
#     CANCELLED = 'cancelled'

# class BugPriority(enum.Enum):
#     LOW = 'low'
#     MEDIUM = 'medium'
#     HIGH = 'high'

class Bug(db.Model):
    __tablename__ = 'bug'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    description = db.Column(db.String(400))
    status = db.Column(db.Integer)
    priority = db.Column(db.Integer)
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, server_default=db.func.now(), server_onupdate=db.func.now())

    def __repr__(self):
        return f'<Bug {self.title}>'

class BugFactory(factory.Factory):
    class Meta:
        model = Bug
    title = factory.fuzzy.FuzzyText(
        length=50, 
        prefix=''
    )
    description = factory.fuzzy.FuzzyText(
        length=100, 
        prefix=''
    )
    status = factory.fuzzy.FuzzyInteger(-2,4)
    priority = factory.fuzzy.FuzzyInteger(0,2)
    created_at = factory.fuzzy.FuzzyDateTime(start_dt=datetime(2019, 1, 1, tzinfo=timezone.utc))
    updated_at = factory.fuzzy.FuzzyDateTime(start_dt=datetime(2019, 1, 1, tzinfo=timezone.utc))

class BugSchema(ma.Schema):
    class Meta:
        fields = (
            'id',
            'title',
            'description',
            'status',
            'priority',
            'created_at',
            'updated_at',
        )

bug_schema = BugSchema()
bugs_schema = BugSchema(many=True)

Params = namedtuple(
    'params',
    [
        'text',
        'priority',
        'status',
        'per_page',
        'page',
        'ordering_column',
        'ordering_direction',
    ]
)

class BugListResource(Resource):

    def _parse_params(self):
        #bad fix because of HttpParams
        params = Params(
            request.args.get('text', None),
            # request.args.get('priority', None),
            int(request.args.get('priority')) if request.args.get('priority', None) else None,
            # request.args.get('status', None),
            int(request.args.get('status')) if request.args.get('status', None) else None,
            # request.args.get('per_page', None),
            int(request.args.get('per_page')) if request.args.get('per_page', None) else None,
            # request.args.get('page', None),
            int(request.args.get('page')) if request.args.get('page', None) else None,
            request.args.get('ordering_column', None),
            request.args.get('ordering_direction', None),
        )
        return params

    def _add_filters(self, query, params):
        if params.text:
            query = query.filter(
                Bug.title.contains(params.text) | Bug.description.contains(params.text)
            )

        if params.priority:
            query = query.filter(Bug.priority == params.priority)

        if params.status:
            query = query.filter(Bug.status == params.status)
        

        return query

    def _add_ordering(self, query, params):
        if not params.ordering_column:
            return query

        if params.ordering_column not in Bug.__table__.columns:
            return query

        if params.ordering_direction == "asc":
            ordering = asc
        elif params.ordering_direction == "desc":
            ordering = desc
        else:
            ordering = asc
        
        query = query.order_by(ordering(params.ordering_column))

        return query


    def _add_paging(self, query, params):
        if params.per_page:
            query = query.filter(
                Bug.title.contains(params.text) | Bug.description.contains(params.text)
            )

        if params.priority:
            query = query.filter(Bug.priority == params.priority)

        if params.status:
            query = query.filter(Bug.status == params.status)

        return query

    def get(self):
        q = Bug.query


        params = self._parse_params()
        print(f"params={params}")

        q = self._add_filters(q, params)
        q = self._add_ordering(q, params)

        paginated_q = q.paginate(
            page=int(params.page) if params.page else None, 
            per_page=int(params.per_page) if params.per_page else None, # default 20
            error_out=True, 
            max_per_page=100,
        )

        pages = int(paginated_q.total / paginated_q.per_page) + (1 if paginated_q.total % paginated_q.per_page else 0)

        # print(f"paginated_q= {paginated_q.__dict__}")

        # return bugs_schema.dump(paginated_q.items)

        return {
            'results' : bugs_schema.dump(paginated_q.items),
            'pagination' : {
                'current_page' : paginated_q.page,
                'next_page' : paginated_q.page + 1 if paginated_q.page < pages else None,
                'previous_page' : None if paginated_q.page == 1 else (paginated_q.page - 1),
                'total' : paginated_q.total,
                'per_page' : paginated_q.per_page,
                'pages' : pages,
            }
        }


    def post(self):
        new_bug = Bug(
            title =request.json["title"],
            description =request.json["description"],
            status =request.json["status"],
            priority =request.json["priority"],
        )
        db.session.add(new_bug)
        db.session.commit()
        return bug_schema.dump(new_bug), 201


class BugResource(Resource):
    def get(self, bug_id):
        bug = Bug.query.get_or_404(bug_id)
        return bug_schema.dump(bug), 200

    def patch(self, bug_id):
        bug = Bug.query.get_or_404(bug_id)
        if 'title' in request.json:
            bug.title = request.json['title']
        if 'description' in request.json:
            bug.description = request.json['description']
        if 'status' in request.json:
            bug.status = request.json['status']        
        if 'priority' in request.json:
            bug.priority = request.json['priority']        

        db.session.commit()
        return bug_schema.dump(bug), 201

    def delete(self, bug_id):
        bug = Bug.query.get_or_404(bug_id)
        db.session.delete(bug)
        db.session.commit()
        return '', 204


api.add_resource(BugListResource, '/bugs')
api.add_resource(BugResource, '/bugs/<int:bug_id>')


@app.after_request # blueprint can also be app~~
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    return response

if __name__ == '__main__':
    db.drop_all()
    db.create_all()
    db.session.add_all(
        [BugFactory() for _ in range(977)  ]
    )
    db.session.commit()
    print(f'\t ==> total bugs created: {Bug.query.count()}')

    app.run(debug=True,host="127.0.0.1",port=7010)
