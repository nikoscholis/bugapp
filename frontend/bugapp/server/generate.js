var faker = require('faker');

var database = { bugs: []};

for (var i = 1; i<= 300; i++) {
  database.bugs.push({
    id: i,
    title: faker.commerce.productName(),
    description: faker.lorem.sentences(),
    created_at: faker.date.between('2019-01-01', '2019-12-06')
  });
}

console.log(JSON.stringify(database));