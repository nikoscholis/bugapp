import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service'

@Component({
  selector: 'app-bugs-cards',
  templateUrl: './bugs-cards.component.html',
  styleUrls: ['./bugs-cards.component.scss']
})
export class BugsCardsComponent implements OnInit {
  title = 'Bugs cards';

  bugs = [];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.get().subscribe( (data: any[]) => {
      console.log(data);
      this.bugs = data;
    })
  }

}
