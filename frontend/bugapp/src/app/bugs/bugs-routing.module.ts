import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
// import { BugsComponent } from './bugs/bugs.component';
import { BugsComponent } from './bugs.component';
import { BugsCardsComponent} from './bugs-cards/bugs-cards.component'
import { BugsTableComponent} from './bugs-table/bugs-table.component'
import { BugsDetailsComponent} from './bugs-details/bugs-details.component'
import { BugsListComponent } from './bugs-list/bugs-list.component';

const routes: Routes = [
    {
        path: 'bugs',
        component: BugsComponent,
        children: [
            {
                path: 'table',
                component: BugsTableComponent
            },
            {
                path: 'cards',
                component: BugsCardsComponent
            },
            {
                path: 'list',
                component: BugsListComponent
            },             
            {
                path: 'details/:id',
                component: BugsDetailsComponent
            },            
        ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class BugsRoutingModule { }