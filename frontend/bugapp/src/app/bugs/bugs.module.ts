import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AngularMaterialModule } from '../angular-material.module'
import { BugsRoutingModule } from './bugs-routing.module'

import { BugsComponent } from './bugs.component';
import { BugsCardsComponent } from './bugs-cards/bugs-cards.component';
import { BugsTableComponent } from './bugs-table/bugs-table.component';
import { BugsDetailsComponent } from './bugs-details/bugs-details.component';
import { FormsModule } from '@angular/forms';
import { BugsListComponent } from './bugs-list/bugs-list.component';


@NgModule({
  declarations: [
    BugsComponent,
    BugsCardsComponent, 
    BugsTableComponent, 
    BugsDetailsComponent, BugsListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AngularMaterialModule,
    BugsRoutingModule
  ]
})
export class BugsModule { }
