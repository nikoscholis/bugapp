import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { DataSource } from '@angular/cdk/collections';
import { Bug } from '../../models/bug.model'
import { BugsService } from '../../services/bugs.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bugs-details',
  templateUrl: './bugs-details.component.html',
  styleUrls: ['./bugs-details.component.scss']
})
export class BugsDetailsComponent implements OnInit {
  title = 'Details';

  bug: Bug;
  bugId: number = 0;

  constructor(
    private bugsService: BugsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.bugsService.get(id).subscribe( 
      result=>{    
        this.bug  =  result;
      }
    )    
  }

}