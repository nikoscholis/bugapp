import { Component, OnInit } from '@angular/core';
import { BugsService } from '../../services/bugs.service';
import { Bug } from '../../models/bug.model'
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-bugs-list',
  templateUrl: './bugs-list.component.html',
  styleUrls: ['./bugs-list.component.scss']
})
export class BugsListComponent implements OnInit {
  
  bugs: Bug;
  selectedBug: Bug;

  constructor(
    private bugsService: BugsService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.bugsService.getAll().subscribe( 
      result=>{    
        this.bugs  =  result;
      }
    )
  }

  
  onSelect(bug: Bug): void {
    this.selectedBug = bug;
  }

}
