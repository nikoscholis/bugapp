import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { DataSource } from '@angular/cdk/collections';
import { Bug } from '../../models/bug.model'
import { Observable, of } from 'rxjs';
import { BugsService, Pagination, BugsServiceParams } from '../../services/bugs.service';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';


export class Paginator {
    pageEvent;
    pageIndex:number;
    pageSize:number;
    length:number;
}

@Component({
    selector: 'app-bugs-table',
    templateUrl: './bugs-table.component.html',
    styleUrls: ['./bugs-table.component.scss']
})
export class BugsTableComponent implements OnInit {
    title = 'Bugs table';

    dataSource: Bug;
    dataSort: MatSort;
    dataPaginator: Paginator = new Paginator;
    servicePagination: Pagination;
    serviceParameters: BugsServiceParams = new BugsServiceParams;
    displayedColumns: string[] = ['id', 'title', 'priority', 'status', 'created_at'];

    isLoadingResults = true;
    isRateLimitReached = false;

    constructor(private bugsService: BugsService) { }

    ngOnInit() {
        this.serviceParameters.per_page = 12;
        this.dataPaginator.length = 12;
    }

    ngAfterViewInit(): void {
        this.retrieveData();
    }

    pageChanged(event) {
        return 1;
    }

    applyFilter(value) {
        if (value.length > 3) {
            this.serviceParameters.text = value;
            this.retrieveData();
        } else {
            this.serviceParameters.text = value;
            this.retrieveData();
        }
    }

    applySorting(event: any) {
        this.serviceParameters.ordering_column = event.active;
        this.serviceParameters.ordering_direction = event.direction;
        this.retrieveData();
    }

    changePage(event) {
        console.log(event);
        this.serviceParameters.page = event.pageIndex;
        this.serviceParameters.per_page = event.pageSize;
        this.retrieveData();
    }

    // applySorting(event: any) {
    //     this.serviceParameters.ordering_column = event.active;
    //     this.serviceParameters.ordering_direction = event.direction;
    //     this.retrieveData();
    // }    

    retrieveData() {
        this.isLoadingResults = true;

        this.bugsService.getAll(this.serviceParameters).subscribe(
            response => {
                console.log(response);
                this.dataSource = response.results;
                this.servicePagination = response.pagination;
                this.isLoadingResults = false;
                this.serviceParameters.page = null;

                this.dataPaginator.pageIndex = this.servicePagination.current_page;
                this.dataPaginator.pageSize = this.servicePagination.per_page;
                this.dataPaginator.length = this.servicePagination.total;

            }
        )
    }

    ngOnDestroy() {
    }
}

// export class BugsDataSource extends DataSource<any> {
//   constructor(private apiService: ApiService) {
//     super();
//   }
//   connect(): Bug[] {
//     this.apiService.get().subscribe( (data: any[]) => {
//       console.log(data);
//       this.bugs = data;
//     })
//     return this.bugs;
//   }
//   disconnect() {}
// }