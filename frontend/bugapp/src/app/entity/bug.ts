export class Bug {
    id: string;
    title: string;
    description: string;
    created_at: string;

    constructor(
        id:string,
        title: string,
        description: string,
        created_at: string
    ) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.created_at = created_at;
    }
}