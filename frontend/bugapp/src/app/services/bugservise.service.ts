import { Injectable } from '@angular/core';
import { Bug } from '../entity/bug';

@Injectable({
  providedIn: 'root'
})
export class BugserviseService {

  allBugs:Bug[] = [
    { 
      "id" : "1",
      "title" : "First bug evaaaa!",
      "description" : "No disc :(",
      "created_at" : "sometime"
    },
    { 
      "id" : "2",
      "title" : "Not so first bug evaaa",
      "description" : "No disc :(",
      "created_at" : "sometime"
    }    
  ];

  // GET
  getBug(id:string):Bug{
    return this.allBugs.find( bug => bug.id == id);
  }

  // GET ALL
  getAllBugs():Bug[]{
    return this.allBugs;
  }

  // UPDATE
  updateBug(bug:Bug){
    var bugToUpdate = this.getBug(bug.id);
    bugToUpdate.title = bug.title;
    bugToUpdate.description = bug.description;
    bugToUpdate.created_at = bug.created_at;
  }

  // DELETE
  deleteBug(id:string){
    this.allBugs = [this.allBugs.find( bug => bug.id != id)];
  }
  

  // constructor() { }
}
