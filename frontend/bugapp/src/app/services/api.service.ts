import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private SERVER_URL = "http://localhost:3000/bugs";
  private apiURL = "http://localhost:3000/bugs";


  constructor(private httpClient: HttpClient) { }

  public get() {
    return this.httpClient.get(this.SERVER_URL);
  }
}


// @Injectable()
// export class UserService {
//   private serviceUrl = 'https://jsonplaceholder.typicode.com/users';
  
//   constructor(private http: HttpClient) { }
  
//   getUser(): Observable<User[]> {
//     return this.http.get<User[]>(this.serviceUrl);
//   }
  
// }