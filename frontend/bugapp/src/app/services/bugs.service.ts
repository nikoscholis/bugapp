import { Injectable } from '@angular/core';
import { 
  HttpClient, 
  HttpHeaders, 
  HttpParams,
  HttpErrorResponse 
} from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Bug } from '../models/bug.model'

export class BugsServiceParams {
  text: string;
  priority: number;
  status: number;
  per_page: number;
  page: number;
  ordering_column: string;
  ordering_direction: string;
}

export class Pagination {
  current_page: number;
  next_page: number;
  previous_page: number;
  total: number;
  per_page: number;
  pages: number;
}

export class PagenatedData<T> {
  results: T;
  pagination: Pagination;
}

@Injectable({
  providedIn: 'root'
})
export class BugsService {

  // API path
  apiURL = 'http://127.0.0.1:7010/bugs';
 
  constructor(private http: HttpClient) { }
 
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }


  // GET
  get(id: number): Observable<Bug> {
    return this.http
      .get<Bug>(this.apiURL + '/' + id)
        .pipe(
            retry(2),
            catchError(this.handleError)
        )
  }

  // GET ALL
  getAll(parameters?: BugsServiceParams): Observable<PagenatedData<Bug>> {
    
    let params = new HttpParams();
    if ( parameters ) {
        if (parameters.text) { params = params.append('text', parameters.text); }
        if (parameters.priority) { params = params.append('priority', parameters.priority.toString()); }
        if (parameters.status) {  params = params.append('status', parameters.status.toString()); }
        if (parameters.per_page) {  params = params.append('per_page', parameters.per_page.toString()); }
        if (parameters.page) {  params = params.append('page', parameters.page.toString()); }
        if (parameters.ordering_column) {  params = params.append('ordering_column', parameters.ordering_column); }
        if (parameters.ordering_direction) {  params = params.append('ordering_direction', parameters.ordering_direction); }
    }

    console.log("parameters = ", parameters );

    return this.http
      .get<PagenatedData<Bug>>(this.apiURL, {params: params})
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
  }

  // CREATE
  create(item: Bug): Observable<Bug> {
    return this.http
      .post<Bug>(this.apiURL, JSON.stringify(item), this.httpOptions)
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
  }

  // UPDATE
  update(id: number, item: Bug): Observable<Bug> {
    return this.http
      .put<Bug>(this.apiURL + '/' + id, JSON.stringify(item), this.httpOptions)
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
  }

  // DELETE
  delete(id: number) {
    return this.http
      .delete<Bug>(this.apiURL + '/' + id, this.httpOptions)
        .pipe(
          retry(2),
          catchError(this.handleError)
        )
  }

  // Error handling
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

}


