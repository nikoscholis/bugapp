from flask import Blueprint
import logging

log = logging.getLogger(__name__)

bp = Blueprint('bugs.bug', __name__)

@bp.route('/bugs', methods=['GET'])
def get_all():

    log.info("GET leme")

    return " hello"
    pass

@bp.route('/bugs/<id>', methods=['GET'])
def get(id):
    pass

@bp.route('/bugs', methods=['POST'])
def create():
    pass

@bp.route('/bugs/<id>', methods=['PUT'])
def update():
    pass

@bp.route('/bugs/<id>', methods=['PATCH'])
def update_partial():
    pass


@bp.route('/bugs/<id>', methods=['DELETE'])
def delete():
    pass

