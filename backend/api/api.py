from flask import Flask
from flask import url_for
from flask import Blueprint
from itertools import chain

import settings

# Endpoints

bp = Blueprint('API', __name__)
@bp.route('/', methods=['GET'])
def base():
    response = '<a href="http://127.0.0.1:7000/bugs">127.0.0.1:7000/bugs</a>'

    return response

class Api:

    def __init__(self):
        self.api = Flask(__name__)

    def setup(self):

        self.api.register_blueprint(bp)
        
        from bugs.api import blueprints as bugs

        for imported_bp in chain(
            bugs
        ):  
            self.api.register_blueprint(imported_bp)


    def run(self):
        self.api.run(
            debug=True, 
            port = settings.API_PORT if hasattr(settings, 'API_PORT') else None
        )
