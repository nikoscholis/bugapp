import sys
import logging

from api import api as app_api




if __name__ == '__main__':

    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s | %(levelname)-8s | %(name)-12s | %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)


    api = app_api.Api()
    api.setup()
    api.run()