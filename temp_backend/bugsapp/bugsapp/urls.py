"""bugsapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from rest_framework import routers
from bugs import views_api as bugs_api_views


_router = routers.DefaultRouter()
_router.register(r'bugs', bugs_api_views.BugViewSet)


# from rest_framework import urls as rest_framework_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('bugs/', include('bugs.urls')),
    # path('api/', include('rest_framework.urls')),
    path('api/', include(_router.urls)),
]
