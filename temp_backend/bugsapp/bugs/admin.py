from django.contrib import admin
from bugs import models


# admin.site.register(models.Bug)

@admin.register(models.Bug)
class BugAdmin(admin.ModelAdmin):
    
    search_fields = ['id', 'title']
    list_display = ['id', 'title' , 'created_at', 'status']
    list_filter = ('priority', 'status' )
    

    readonly_fields= [
        'created_at',
        'created_by',
        'modified_at',
        'modified_by'
    ]