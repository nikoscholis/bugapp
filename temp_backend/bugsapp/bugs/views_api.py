from django.shortcuts import render
from rest_framework import viewsets
from bugs.serializers import BugSerializer
from bugs import models

# Create your views here.

class BugViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.Bug.objects.all().order_by('-created_at')
    serializer_class = BugSerializer
