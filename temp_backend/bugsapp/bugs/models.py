from django.db import models
from django.contrib.auth.models import User

BUG_STATUS = [
    (-1,'Deleted'),
    (0,'Created'),
    (1,'Pending'),
    (2,'In Progress'),
    (3,'Done'),
    (4,'Cancelled'),
]

BUG_PRIORITY = [
    (0,'Low'),
    (1,'Medium'),
    (2,'High'),
]

class Bug(models.Model):
    title					= models.CharField(max_length=50)
    description				= models.CharField(max_length=400, null=True, blank=True)
    reporter                = models.ForeignKey(User, related_name='reporter', on_delete = models.SET_NULL, null=True)
    assignee                = models.ForeignKey(User, related_name='assignee', on_delete = models.SET_NULL, null=True)
    status					= models.SmallIntegerField(default=0, choices=BUG_STATUS)
    priority				= models.SmallIntegerField(default=0, choices=BUG_PRIORITY)
    created_at       		= models.DateTimeField(auto_now=True)
    created_by              = models.ForeignKey(User, related_name='created_by', on_delete = models.SET_NULL, null=True, editable=False)
    modified_at       		= models.DateTimeField(auto_now=True)
    modified_by             = models.ForeignKey(User, related_name='modified_by', on_delete = models.SET_NULL, null=True, editable=False)

    class Meta:
        verbose_name = 'Bug'
        verbose_name_plural = 'Bugs'
        ordering = ['-created_at']

    def __str__(self):
        return str(self.pk) + ': ' + self.title + ' from ' + self.assignee.username if self.assignee else ' - '



