from bugs import models
from rest_framework import serializers
from bugsapp import serializers as bugsapp_serializers

class BugSerializer(serializers.HyperlinkedModelSerializer):
    created_by = bugsapp_serializers.UserSerializer()
    modified_by = bugsapp_serializers.UserSerializer()

    class Meta:
        model = models.Bug

        fields = [
            'title',
            'description',
            # 'reporter',
            # 'assignee',
            'status',
            'priority',
            'created_at',
            'created_by',
            'modified_at',
            'modified_by',
        ]
    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        print(f' ==== validated_data={validated_data}')
        print(f' ==== self.__dict__={self.__dict__}')
        print(f' ==== request={self.request}')
        return models.Bug.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.status)
        instance.status = validated_data.get('status', instance.description)
        instance.priority = validated_data.get('priority', instance.priority)
        instance.save()
        return instance                


# class BugSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     title = serializers.CharField(required=False, allow_blank=True, max_length=50)
#     description = serializers.CharField(required=False, allow_blank=True, max_length=400)

#     status = serializers.ChoiceField(choices=models.BUG_STATUS, default=0)
#     priority = serializers.ChoiceField(choices=models.BUG_PRIORITY, default=0)

#     created_by = bugsapp_serializers.UserSerializer
#     modified_by = bugsapp_serializers.UserSerializer
    
#     def create(self, validated_data):
#         """
#         Create and return a new `Snippet` instance, given the validated data.
#         """
#         print(f' ==== validated_data={validated_data}')
#         print(f' ==== self.__dict__={self.__dict__}')
#         print(f' ==== request={self.request}')
#         return models.Bug.objects.create(**validated_data)

#     def update(self, instance, validated_data):
#         """
#         Update and return an existing `Snippet` instance, given the validated data.
#         """
#         instance.title = validated_data.get('title', instance.title)
#         instance.description = validated_data.get('description', instance.status)
#         instance.status = validated_data.get('status', instance.description)
#         instance.priority = validated_data.get('priority', instance.priority)
#         instance.save()
#         return instance        